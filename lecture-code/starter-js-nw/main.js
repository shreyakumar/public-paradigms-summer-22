console.log('js api demo page loaded');

var submitButton = document.getElementById('submit-button');
submitButton.onmouseup = getFormInfo; //

function getFormInfo(){ // this is triggered when the button is clicked
  // get name from user
  var name = document.getElementById('name-text').value;
  makeNetworkCallToAgeApi(name);

} // end of fn getFormInfo

function makeNetworkCallToAgeApi(name){
  var url = "https://api.agify.io/?name=" + name;

  var xhr = new XMLHttpRequest();
  xhr.open("GET", url, true);

  // make nw call to agify api
  // get response from agify
  xhr.onload = function(e){ // this is triggered when the response for age api comes back
    // get the response from the api
    var response = xhr.responseText;
    console.log('response is ' + response);
    updateAgewithResponse(name, response);
  } // end of anonymous fn associated with onload

  xhr.onerror = function(e){
    // TODO
    console.error('error message is ' + xhr.statusText);
  } // end of anonymous fn associated with onerror

  xhr.send(null);
} // end of makeNetworkCallToAgeApi

function makeNetworkCallToNumbersApi(age){
  // TODO
    console.log('entered makeNetworkCallToNumbersApi');
    var url = "http://numbersapi.com/" + age;

    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);

    xhr.onload = function(e) {
      var response = xhr.responseText;
      console.log('response is ' + response);
      updateUIwithresponse(response);
    }

    xhr.onerror = function(e){
        console.error(xhr.statusText);
    }

    xhr.send(null); // send request without bosy
} // end of makeNetworkCallToNumbersApi

function updateUIwithresponse(response){
  // option 1:
  var label2 = document.getElementById('response-line-2');
  label2.innerHTML = response;

  // option 2: dynamically create a new element and attach it to the dom
  var item_text = document.createTextNode(response);
  var label_item = document.createElement("label");
  label_item.appendChild(item_text);
  var response_div = document.getElementById('response-area');
  response_div.appendChild(label_item);


} //

function updateAgewithResponse(name, response){
  // update the UI with age
  var response_json = JSON.parse(response);
  var label1 = document.getElementById('response-line-1');

  if (response_json['age'] != null){
    var age = parseInt(response_json["age"]);
    var string_message = name + ", your predicted age is " + age + ".";
    label1.innerHTML = string_message;
    makeNetworkCallToNumbersApi(age);

  } else {
    label1.innerHTML = 'Apologies, no data.';
  }

  // make api call to numbers with age
  //makeNetworkCallToNumbersApi(age);
  // show result to UI
  // get age in a var
  // then make nw call to numbers with age
  // show trivia of number age

}
