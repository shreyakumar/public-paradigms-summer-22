package com.example.shreya_simpleapp_su22;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ContactActivity extends AppCompatActivity {

    private TextView mDisplayAboutTextView;
    private Button mOpenWebpageButton;
    private Button mOpenMapButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact); // connected to activity_contact.xml file

        // connect with UI elements
        mDisplayAboutTextView = (TextView) findViewById(R.id.tv_about_text);
        mOpenWebpageButton = (Button) findViewById(R.id.button_open_webpage);
        mOpenMapButton = (Button) findViewById(R.id.button_open_map);

        // grabbing the data that the originating intent sends us
        Intent intentThatStartedThisActivity = getIntent();
        String message = "news";
        //check is extra data
        if(intentThatStartedThisActivity.hasExtra(Intent.EXTRA_TEXT)){
            message = intentThatStartedThisActivity.getStringExtra(Intent.EXTRA_TEXT);
            mDisplayAboutTextView.append("\n\n\n" + message);
        } // end if

        final String urlString = "https://www.nd.edu/"; // url string

        // open webpage button
        mOpenWebpageButton.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){

                        openWebPage(urlString);

                    } // end of onClick method

                } // end of View
        ); // end of setOnClickListener

        // open map button
        mOpenMapButton.setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){

                        openMap();

                    } // end of onClick
                } // end of View
        ); // end of setOnCli

    } // end of onCreate

    // helper functions
    public void openMap(){
        Log.d("DEBUG", "open map function triggered!!!!!!!!!!");
        String addressString = "University of Notre Dame, IN";
        Uri addressUri = Uri.parse("geo:0,0").buildUpon().appendQueryParameter("q", addressString).build();
        Intent openMapIntent = new Intent(Intent.ACTION_VIEW);
        openMapIntent.setData(addressUri);

        startActivity(openMapIntent); // actually launch activity
    } // end of open map

    public void openWebPage(String urlString){
        Log.d("DEBUG", "open web page function triggered!!!!!!!!!!");

        Uri webpage = Uri.parse(urlString);

        Intent openWebPageIntent = new Intent(Intent.ACTION_VIEW, webpage);
        startActivity(openWebPageIntent);
    } // end of open web page



} // end of ConactActivity class