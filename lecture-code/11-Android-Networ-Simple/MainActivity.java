package com.example.shreya_simpleapp_su22;

import com.example.shreya_simpleapp_su22.utilities.NetworkUtils;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.Button;

import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private TextView mSearchResultsDisplay;
    private EditText mSearchTermEditText;
    private Button mSearchButton;
    private Button mResetButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // connect with UI elements
        mSearchResultsDisplay   = (TextView) findViewById(R.id.tv_search_results);
        mSearchTermEditText     = (EditText) findViewById(R.id.et_search_term);
        mSearchButton           = (Button) findViewById(R.id.button_search);
        mResetButton            = (Button) findViewById(R.id.button_reset);

        //mSearchResultsDisplay.append("Dwight" + "\n\n" + "Pam" + "\n\n");

        final String[] studentNames = {"Tara", "Mia", "Meng", "Carlo", "Thomas", "Mo"};
        for(String name : studentNames){
            mSearchResultsDisplay.append("\n\n"+ name);
        }

        final String defaultDisplayText = mSearchResultsDisplay.getText().toString(); // saving string to use later

        mSearchButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        makeNetworkSearchQuery();
                    } // end of onClick
                } // end of View.OnClickListener

        ); // end of setOnClickListener

        mResetButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mSearchResultsDisplay.setText(defaultDisplayText);
                    } // end of onClick
                } // end of View.OnClickListener
        ); // end of setOnClickListener

    } // end of onCreate

    /* Networking related code begins */
    public void makeNetworkSearchQuery(){
        // get the search string
        String searchTerm = mSearchTermEditText.getText().toString();
        //reset the search results
        mSearchResultsDisplay.setText("Results : \n\n");
        // make the search - network
        new FetchNetworkData().execute(searchTerm);

    } // end of makeQuery

    // inner class
    public class FetchNetworkData extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params){
            //get the search term
            if(params.length == 0) return null;
            String searchTerm = params[0];
            // get the Url
            URL searchUrl = NetworkUtils.buildCountriesUrl(); // write class and method

            // get the response from the URl
            String responseString = null;
            try{
                responseString = NetworkUtils.getResponseFromUrl(searchUrl); //  write this method
            }catch(Exception e){
                e.printStackTrace();
            }
            return responseString;//
        } // end of doInBackground

        @Override
        protected void onPostExecute(String responseData){
            ArrayList<String> titles = NetworkUtils.parseCountriesJson(responseData); //
            // display entries in GUI
            for(String title: titles){
                mSearchResultsDisplay.append("\n\n" + title);
            }
        } // end of onPost
    } // end of inner class

    /* Networking related code ends */


    // how to connect with menu
    // Steps: go to About page from menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.main_menu, menu); // id of the menu resource that should be inflated
        return true;
    } // end of onCreateOptionsMenu

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int menuItemSelected = item.getItemId();

        if(menuItemSelected == R.id.menu_contact){ // id from main_menu.xml for the About item


            //spl - launching activity in our app - then launch the About Activity
            Class destinationActivity = ContactActivity.class;

            // create intent to go to next page
            Intent startAboutActivityIntent = new Intent(MainActivity.this, destinationActivity);

            String msg = mSearchTermEditText.getText().toString();
            startAboutActivityIntent.putExtra(Intent.EXTRA_TEXT, msg);

            startActivity(startAboutActivityIntent);
            Log.d("info", "Contact Activity launched");
        } // end if
        return true;
    } // end of onOptions


} // end of class