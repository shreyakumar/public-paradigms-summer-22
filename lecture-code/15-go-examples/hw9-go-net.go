package main

import (
	"fmt"
	"net/http"
	"bufio"
)

func queryApi(urlString string, c chan string) {
    //TODO use http.Get to make a connection to urlString. catch the response in resp and err

	//DONE print a message if there is an error with the connection
    if err != nil {
        fmt.Println("connection error: ", err)
    }

    //TODO create a new scanner using bufio.NewScanner() for the resp.Body
	//TODO check if scanner has something to read, and read it with scanner.Text()
    // TODO save the text read by scanner onto the channel
    line, err := reader.ReadString('\n')

    //TODO check if scanner worked, if not, print the scanner error
	if err := scanner.Err(); err != nil {
    	fmt.Println("scanner error: ", err)
    }
} // end of queryApi

func main() {
    //TODO first make a channel for strings
	//TODO set up the query strings for both apis

    //TODO call queryApi in a goroutine once for each query
	//Note: you should hard code the name and age values in the url string

    //TODO read from the channel twice, once to collect data from each goroutine
    // remember that the channel will block on the read until the channel is written to by one of the goroutines
    //TODO save the responses as string variables resp1 and resp2

    //DONE print out the resp1 and resp2
    fmt.Println("=================== RESPONSE 1 ===================\n")
    fmt.Println(resp1)
    fmt.Println("\n=================== RESPONSE 2 ===================\n")
    fmt.Println(resp2)
    fmt.Println("\n==================================================")

    // correct output will have the text of the HTTP response from both requests
    // note that the ORDER might vary depending on which goroutine writes to the channel first
} // end of main
