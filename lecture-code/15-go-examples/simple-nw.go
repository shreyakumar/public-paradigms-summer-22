package main

import (
    "fmt"
    "bufio"
    "net/http"
)

func main() {
    urlString := "https://api.agify.io/?name=" + "Bagheera"

    resp, err := http.Get(urlString) // makes the GET request

    if err != nil {
        fmt.Println("connection error: ", err)
    }
    defer resp.Body.Close() // this happens at the end of our scope

    //Print the HTTP response status.
    fmt.Println("Response status:", resp.Status)
    scanner := bufio.NewScanner(resp.Body) //use scanner to parse the response
    if scanner.Scan() { // this grabs the response into scanner.Text()
        fmt.Println("first line of response:", scanner.Text())
    }

    if err := scanner.Err(); err != nil {
        fmt.Println("scanner error: ", err)
    }
} // end of main
