public class BankDemo{
   public static void main(String[] args){
      Checking ac1 = new Checking("A123", "Dwight Schrute", 10000); // new account object
      System.out.println(ac1.getAcNum() + " created for " + ac1.getCustName() + " has a limit of " +  ac1.getWLimit());
    } // end of main
} // end of class
