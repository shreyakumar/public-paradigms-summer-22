package com.example.lecture01_simpleandroidapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    // connect with visual UI component
    TextView mResultsDisplay;
    Button mSearchButton;
    Button mResetButton;
    EditText mSearchTermEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main); // now this class is connected to activity_main.xml file

        mResultsDisplay = (TextView)findViewById(R.id.tv_search_results);
        mSearchButton = (Button)findViewById(R.id.button_search);
        mResetButton = (Button) findViewById(R.id.button_reset);
        mSearchTermEditText = (EditText)findViewById(R.id.et_search_term);

        // TODO: manipulate connected UI component
        //mResultsDisplay.append("\n\nSofia\n\nMeng\n\nMia");
        String[] studentNames = {"Tara", "Mack", "Sofia", "Jozef", "Thomas"};
        mResultsDisplay.append("\n\n");
        for(String name: studentNames){
            Log.d("DEBUG", "--------------------------Name added: " + name);
            mResultsDisplay.append(name + "\n\n");
        } // end of for loop

        String defaultDisplayText = mResultsDisplay.getText().toString(); // to be used by the reset button

        mSearchButton.setOnClickListener(
                new View.OnClickListener() { // this creates an unnamed object
                    @Override
                    public void onClick(View view) {
                        // this code is triggered when the button is clicked
                        // grab the user's search term
                        String searchTerm = mSearchTermEditText.getText().toString();

                        boolean nameFound = false;
                        // compare with our list of student names
                        for(String name: studentNames){
                            if(name.toLowerCase().equals(searchTerm.toLowerCase())){
                                mResultsDisplay.setText(name);
                                nameFound = true;
                                break;
                            } // end of if
                        } // end of for

                        if(!nameFound){
                            mResultsDisplay.setText("Name not found.");
                        }
                        // then show result of either found name or no results found
                    } // end of onClick() method inside the View.OnClickListener
                } // end of View.OnClickListener
        ); // end of setOnClickListener

        mResetButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.d("DEBUG", "-----------------clicked on reset button!");
                        // set the results display to be the original on load text
                        //mResultsDisplay.setText(defaultDisplayText);
                        // use Intent to move to second page - contact activity
                        Class destinationActivity = ContactActivity.class; // destination
                        Intent startContactActivityIntent = new Intent(MainActivity.this, destinationActivity); // this intent has the ability to move from one activity to another
                        // it does not actually move yet

                        // message to send to next activity
                        String msg = mSearchTermEditText.getText().toString(); // "hello from page 1";
                        startContactActivityIntent.putExtra(Intent.EXTRA_TEXT, msg);

                        Log.d("DEBUG", "about to trigger switch to next activity");
                        startActivity(startContactActivityIntent); // this will actually trigger the switch to contact

                    } // end of onClick
                } // end of View.OnClickL
        ); // end of setOnClickListener



    } // end of onCreate
}