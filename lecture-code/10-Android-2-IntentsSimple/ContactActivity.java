package com.example.lecture01_simpleandroidapp;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ContactActivity extends AppCompatActivity {

    TextView mMessagetextView;
    Button mOpenMapButton;
    Button mOpenWebpageButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact); // connecting to xml UI

        mMessagetextView = (TextView) findViewById(R.id.tv_message);
        mOpenMapButton = (Button)findViewById(R.id.button_open_map);
        mOpenWebpageButton = (Button)findViewById(R.id.button_open_webpage);

        Log.d("DEBUG", "ContactActivity launched!");

        // grab the message from the Intent that launched me
        Intent intentThatStartedThisActivity = getIntent(); // this returns the intent that launched us
        String incomingMessage = "Paradigms!";
        if(intentThatStartedThisActivity.hasExtra(Intent.EXTRA_TEXT)){
            incomingMessage = intentThatStartedThisActivity.getStringExtra(Intent.EXTRA_TEXT);
            mMessagetextView.append("\n\n\n" + incomingMessage);
        }

        String urlString = "https://www.nd.edu/";

        // open map button listener
        mOpenMapButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openMap(); // we will write this function
                    } // end of onClick - for open Map
                }
        );

        // open webpage button listener
        mOpenWebpageButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        openWebpage(urlString); // we will write this function
                    } // end of onClick - for open Map
                }
        );


    } // end of onCreate

    public void openMap(){
        //  - open map address for ND
        String addressString = "University of Notre Dame, IN";
        Uri addressUri = Uri.parse("geo:0,0").buildUpon().appendQueryParameter("q", addressString).build();
        Intent openMapIntent = new Intent(Intent.ACTION_VIEW);
        openMapIntent.setData(addressUri);

        startActivity(openMapIntent); // this actually launches the second intent - some mapping application
    } // end of open map

    public void openWebpage(String urlString){
        //TODO - open the nd.edu page in a browser
        Uri webpageUri = Uri.parse(urlString);

        Intent openWebpageIntent = new Intent(Intent.ACTION_VIEW, webpageUri);
        startActivity(openWebpageIntent);

    } // end of open webpage
} // end of class